<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class AirportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('seeders/airports.json');
        $data = json_decode(File::get($file), true);

        foreach ($data as $abbreviation => $airport) {
            DB::table('airports')->insert([
                'abbreviation' => $abbreviation,
                'city_en_name' => $airport['cityName']['en'],
                'city_ru_name' => $airport['cityName']['ru'] ?? null,
                'airport_en_name' => $airport['airportName']['en'] ?? null,
                'airport_ru_name' => $airport['airportName']['ru'] ?? null,
                'area' => $airport['area'] ?? null,
                'country' => $airport['country'],
                'timezone' => $airport['timezone'],
                'lat' => $airport['lat'] ?? null,
                'lng' => $airport['lng'] ?? null,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
