<?php

namespace App\Repositories;

use App\Models\Airport;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Support\Collection;

class AirportRepository extends BaseRepository
{
    public function __construct(Airport $model)
    {
        $this->model = $model;
    }

    /**
     * @param string $airportName
     * @return Collection
     */
    public function getByName(string $airportName): Collection
    {
        return $this->model->where("airport_ru_name", "like", "%$airportName%")->get();
    }
}
