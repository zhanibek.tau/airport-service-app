<?php

namespace App\Services;

use App\Repositories\AirportRepository;
use Illuminate\Database\Eloquent\Collection;

class AirportService
{
    private AirportRepository $airportRepository;

    public function __construct(
        AirportRepository $airportRepository
    )
    {
        $this->airportRepository = $airportRepository;
    }

    /**
     * @param string $airportName
     * @return Collection
     */
    public function getAirportByName(string $airportName): \Illuminate\Database\Eloquent\Collection
    {
        return $this->airportRepository->getByName($airportName);
    }
}
