<?php

namespace App\Exceptions;

use App\Traits\ResponseTrait;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    use ResponseTrait;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    public function report(Throwable $exception): void
    {
        if (app()->bound('sentry') && $this->shouldReport($exception)) {
            app('sentry')->captureException($exception);
        }

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Throwable $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $e)
    {
        if ($e instanceof ValidationException) {
            return $this->response($e->errors(), 422, 'Validation error', false, []);
        }

        $code = 500;
        $trace = json_decode(json_encode($e->getTrace(), JSON_INVALID_UTF8_IGNORE));
        $message = json_encode($e->getMessage());

        Log::error($message, ['exception' => $e->getTrace()]);

        switch (true) {
            case $e instanceof ModelNotFoundException:
                $code = 404;
                $message = 'Record not found';
                break;
            case $e instanceof NotFoundHttpException:
                $trace = [];
                $message = 'Not found';
                break;
            case $e instanceof NoticeException:
                break;
            case $e instanceof AuthenticationException:
                $message = 'Authentication error';
                $code = 401;
                $trace = [];
                break;
            case $e instanceof AuthorizationException:
                $message = 'Authorization required';
                $code = 403;
                $trace = [];
                break;
            case $e instanceof MethodNotAllowedHttpException:
                $message = 'Method not allowed';
                $code = 405;
                $trace = [];
                break;
            default:
                break;
        }

        if (env('APP_ENV') == 'prod' || app()->isProduction()) {
            return $this->response([], $code, $message, false, $trace);
        }

        return $this->response([], $code, $message, false, $trace);
    }
}
