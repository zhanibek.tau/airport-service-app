<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Airport extends Model
{
    use HasFactory;

    protected $connection = 'mysql';

    protected $table = 'airports';

    protected $fillable = [
        'abbreviation',
        'city_en_name',
        'city_ru_name',
        'airport_ru_name',
        'airport_en_name',
        'area',
        'country',
        'timezone',
        'lat',
        'lng',
        'created_at',
        'updated_at',
    ];
}
