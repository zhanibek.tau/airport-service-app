<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AirportResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id'                        => $this->id,
            'abbreviation'              => $this->abbreviation,
            'city_en_name'              => (string) $this->city_en_name,
            'city_ru_name'              => (string) $this->city_ru_name,
            'airport_ru_name'           => (string) $this->airport_ru_name,
            'airport_en_name'           => (string) $this->airport_en_name,
            'area'                      => (string) $this->area,
            'country'                   => (string) $this->country,
            'timezone'                  => (string) $this->timezone,
            'lat'                       => (float) $this->lat,
            'lng'                       => (float) $this->lng,
        ];
    }
}
