<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *    type="object",
 *    title="AirportRequest",
 *    description="Airport request params",
 *
 *    @OA\Property(
 *      property="airport_name",
 *      title="airport_name",
 *      type="string"
 *    ),
 *    example={"airport_name": "Эль-Меллах"}
 * )
 */
class AirportRequest  extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array[]
     */
    public function rules(): array
    {
        return [
            'airport_name' => [
                'required',
                'string',
            ],
        ];
    }
}
