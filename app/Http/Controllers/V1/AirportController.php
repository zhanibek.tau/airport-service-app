<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\AirportRequest;
use App\Http\Resources\AirportResource;
use App\Services\AirportService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;

class AirportController extends Controller
{
    private AirportService $airportService;

    public function __construct(
        AirportService $airportService
    )
    {
        $this->airportService = $airportService;
    }

    /**
     *  @OA\Post(
     *     path="/airport/v1/airport/get-airport-by-name",
     *     tags={"airport v1"},
     *     summary="Recieving airport's info by name of airport",
     *     @OA\RequestBody(
     *       @OA\JsonContent(ref="#/components/schemas/AirportRequest")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Order payment successefully placed in the shop",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="status",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="details",
     *                     type="object",
     *                     @OA\Property(
     *                          property="response",
     *                          type="string"
     *                     ),
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Invalid payload or code error is happened",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Response(
     *         response="422",
     *         description="Validation error",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Full authentication is required to access this resource",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Access forbidden",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     * )
     * @param AirportRequest $request
     * @return JsonResponse|Collection
     */
    public function getAirportByName(AirportRequest $request): JsonResponse|Collection
    {
        $requestData = $request->validated();

        return $this->response(AirportResource::collection($this->airportService->getAirportByName($requestData['airport_name'])));
    }
}
