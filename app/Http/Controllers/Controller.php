<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Traits\ResponseTrait;
use Illuminate\Routing\Controller as BaseController;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Airport Service (API)",
 *      description="<b>Описание:</b> Сервис включает функционал по получению информации по аэропортам",
 * )
 *
 * @OA\Tag(
 *     name="Airport Service",
 *     description="API Endpoints of Airport"
 * )
 *
 */
class Controller extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;
    use ResponseTrait;
}

