<?php

use App\Http\Controllers\Healthcheck;
use App\Http\Controllers\V1\AirportController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::prefix('airport')->group(function () {
    /**
     * Healthchecks for users and kubernetes
     */
    Route::prefix('healthcheck')->group(function () {
        Route::get('healthcheck', [Healthcheck::class, 'healthcheck']);
        Route::get('liveness', [Healthcheck::class, 'liveness']);
        Route::get('readiness', [Healthcheck::class, 'readiness']);
    });

// v1
    Route::prefix('v1')->group(function () {
        Route::prefix('airport')->group(function () {
            Route::post('get-airport-by-name', [AirportController::class, 'getAirportByName'])->name('ecom.result');
        });
    });
});
